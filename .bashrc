export PATH=$PATH":$HOME/.local/bin"
#
# ~/.bashrc
#
alias vim='nvim'
alias c='tty-clock -c'
alias yta='youtube-dl --extract-audio --audio-format mp3'
alias ytv='youtube-dl --add-metadata -i'
alias storage='cd /run/media/kylinux/storagessd/'
alias rstorage='ranger /run/media/kylinux/storagessd/'
alias x='7z x'
alias ios='idevicepair pair && cd /run/user/1000/gvfs'
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
