#!/bin/bash

cp -rf i3/config ~/.config/i3/
cp -rf i3status/config ~/.config/i3status/
cp -rf nvim/init.vim ~/.config/nvim/
cp -rf ranger/ ~/.config/ranger/
cp -rf .bash_profile ~/
cp -rf .bashrc ~/
cp -rf .Xresources ~/
cp -rf .xinitrc ~/
